Lamplid = cc.Node.extend({
	ctor:function(p){
		this._super();
		p.addChild(this, 10, TAG_LAMPLID_NODE);
		this.init();
	},

	init : function(){
		var lamp =  new Button(this, 9, TAG_LAMPLID, "#lamp/lp2.png",this.callback);

	},
	callback:function(p){
		var action = gg.flow.flow.action;
		var func = cc.callFunc(this.actionDone, this);	
		func.retain();
		switch(p.getTag()){	
		case TAG_LAMPLID:
			if (action==ACTION_DO1){
				hand.addrighthand(this,"#hand/hand_right",cc.p(20,-15));
				var move = cc.moveTo(0.5,cc.p(0,50));
				var ber = cc.bezierTo(1,[cc.p(30,30),cc.p(50,0),cc.p(80,-15)]);	
				var seq = cc.sequence(move,ber,cc.callFunc(function(){
					hand.removehand(this,1,2);
					this.flowNext();
				},this));
				this.runAction(seq);
				
			}
			else if (action==ACTION_DO2){
				var fire = this.getParent().getChildByTag(TAG_LAMP_NODE).getChildByTag(TAG_SHOW);
				
				hand.addrighthand(this,"#hand/hand_right",cc.p(20,-15));
				var move = cc.moveTo(0.5,cc.p(0,25));
				var move1 = cc.moveTo(0.5,cc.p(0,50));
				var ber = cc.bezierTo(1,[cc.p(50,0),cc.p(30,30),cc.p(0,80)]);	
				var seq = cc.sequence(ber,cc.callFunc(function(){
					fire.runAction(cc.scaleTo(0.5,0.3,0));
				},this),move,move1,move,cc.callFunc(function(){
					hand.removehand(this,1,2);
				},this),cc.delayTime(3),cc.callFunc(function(){	
					
					this.flowNext();
				},this));
				this.runAction(seq);
				
			}
		}
		
	},

	actionDone:function(p){
		var action = gg.flow.flow.action;
		var func = cc.callFunc(this.actionDone, this);
		switch(p.getTag()){
		case TAG_LAMPLID:
			if (action==ACTION_DO1){

			}
			else if (action==ACTION_DO2){

			}
		}
	},
	flowNext:function(){
		gg.flow.next();
	},

});