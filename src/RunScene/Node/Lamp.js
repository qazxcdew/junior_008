Lamp = cc.Node.extend({
	ctor:function(p){
		this._super();
		p.addChild(this, 10, TAG_LAMP_NODE);
		this.init();
	},

	init : function(){
		var lamp =  new Button(this, 9, TAG_LAMP, "#lamp/lp1.png",this.callback);
	
	},
	callback:function(p){
		var action = gg.flow.flow.action;
		var func = cc.callFunc(this.actionDone, this);	
		func.retain();
		switch(p.getTag()){	
		case TAG_LAMP:
			if(action==ACTION_DO1){
				var clock = ll.run.clock;
				clock.setSpriteFrame("tool/clock1.png");
				var zhengliunode = ll.run.getChildByTag(TAG_ZHENGLIU_NODE);
				hand.addrighthand(this,"#hand/hand_right",cc.p(20,-25));
				var seq = cc.sequence(cc.moveTo(1,cc.p(-420,0)),cc.callFunc(function(){
					hand.removehand(this, 1, 2);
					zhengliunode.addwendu();
					zhengliunode.schedule(function(){
						zhengliunode.addqipao(cc.p(zhengliunode.addposX(),115),0.5);
					}, 0.1, 40, 3);
					clock.doing();
				},this),cc.callFunc(function(){
					var shuizhu = new cc.Sprite("#shuizhu.png");
					shuizhu.setScale(0.3);
					shuizhu.setPosition(cc.p(-50,160));
					zhengliunode.addChild(shuizhu);
					shuizhu.setOpacity(0);
					var seq = cc.sequence(cc.delayTime(5),cc.fadeIn(2));
					shuizhu.runAction(seq);
				},this),cc.delayTime(7),cc.callFunc(function(){
					zhengliunode.schedule(function(){
						zhengliunode.addqipao(cc.p(zhengliunode.addposX(),115),0.3);
					}, 0.05, cc.REPEAT_FOREVER);									
				},this),cc.delayTime(1),cc.callFunc(function(){
					clock.stop();
					var show = new ShowTip(ll.run,"观察现象",25,cc.p(900,200));
					zhengliunode.schedule(function(){//水滴慢
						zhengliunode.genPoint(0.5);
					}, 2, 10);	
					
					var waterflow = new cc.Sprite("#shuiliu/1.png");
					waterflow.setScale(0.3);
					waterflow.setPosition(235,115);
					zhengliunode.addChild(waterflow);
					waterflow.runAction(cc.repeatForever(zhengliunode.waterflow()));					
					zhengliunode.addwater();
				},this),cc.delayTime(10),cc.callFunc(function(){
					zhengliunode.schedule(function(){//水滴快
						zhengliunode.genPoint(0.3);
					}, 0.8, 10);
					
				},this),cc.delayTime(10),cc.callFunc(function(){
					this.flowNext();
				},this));
			
				this.runAction(seq);
			}
			else if(action==ACTION_DO2){
				var zhengliunode = ll.run.getChildByTag(TAG_ZHENGLIU_NODE);
				hand.addrighthand(this,"#hand/hand_right",cc.p(20,-25));
			    var seq = cc.sequence(cc.moveTo(1,cc.p(0,0)),cc.callFunc(function(){
			    	hand.removehand(this, 1, 2);
			    	this.flowNext();
			    },this),cc.delayTime(2),cc.callFunc(function(){
			    	zhengliunode.stopAll();
			    },this));
			    
				this.runAction(seq);
			}
			
		}

	},

	actionDone:function(p){
		var action = gg.flow.flow.action;
		var func = cc.callFunc(this.actionDone, this);
		switch(p.getTag()){
		case TAG_LAMP:
		}
	},
	flowNext:function(){
		gg.flow.next();
	},

});