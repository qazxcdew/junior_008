Zhengliu = cc.Node.extend({
	posx:160,
	posy:160,
	ctor:function(p){
		this._super();
		p.addChild(this, 8, TAG_ZHENGLIU_NODE);
		this.init();
	},

	init : function(){		
		var iron =  new Button(this, 1, TAG_IRON, "#iron.png",this.callback);
		//var iron = new cc.Sprite("#iron.png");//铁架台
		iron.setPosition(-68,145);
		iron.setScale(0.3);
	//	this.addChild(iron,1);
		
		var iron1 =  new Button(this, 1, TAG_IRON1, "#iron1.png",this.callback);
		//var iron1 = new cc.Sprite("#iron1.png");//铁架台
		iron1.setPosition(115,145);
		iron1.setScale(0.3);
		//this.addChild(iron1,1);
		
		var iron2 = new cc.Sprite("#iron2.png");//铁架台
		iron2.setPosition(102,184);
		iron2.setScale(0.3);
		this.addChild(iron2,6);
		
		var feishi = new cc.Sprite("#feishi.png");//沸石
		feishi.setPosition(-50,122);
		feishi.setScale(0.15,0.1);
		this.addChild(feishi,1);
		
		var jiazi =  new Button(this, 6, TAG_JIAZI, "#jiazi.png",this.callback);
		//var net = new cc.Sprite("#jiazi.png");//蝴蝶夹
		jiazi.setPosition(-95,215);
		jiazi.setScale(0.3);
		//this.addChild(net,6);
		
		var jieshouqi =  new Button(this, 6, TAG_JIESHOUQI, "#jieshouqi.png",this.callback);
	//	var jieshouqi = new cc.Sprite("#jieshouqi.png");//接收器
		jieshouqi.setPosition(230,115);
		jieshouqi.setScale(0.3);
		//this.addChild(jieshouqi,6);				
		
		var zhengliu =  new Button(this, 2, TAG_ZHENGLIU, "#zhengliu.png",this.callback);//蒸馏烧瓶
		//var zhengliu = new cc.Sprite("#zhengliu.png");
		zhengliu.setPosition(-43,200);
		zhengliu.setScale(0.3);
	//	this.addChild(zhengliu,1,TAG_ZHENGLIU);
		
		var down =  new Button(this, 2, TAG_DOWN, "#down.png",this.callback);//进水口
		down.setPosition(143,147);
		down.setRotation(3);
		down.setScale(0.3);
		
		
		var lengning =  new Button(this, 5, TAG_LENGNINGGUAN, "#lengning.png",this.callback);//进水口
		//var lengning = new cc.Sprite("#lengning.png");//冷凝管
		lengning.setPosition(90,190);
		lengning.setScale(0.3);
		lengning.setRotation(20);
		//this.addChild(lengning,5);
		
		var zhuixing =  new Button(this, 0, TAG_BEAKERFLASK, "#zhuixing.png",this.callback);//进水口
		//var zhuixing = new cc.Sprite("#zhuixing.png");//锥形瓶
		zhuixing.setPosition(270,50);
		zhuixing.setScale(0.3);
		//this.addChild(zhuixing);
		
		var wendu = new cc.Sprite("#wendu.png");
		wendu.setPosition(-50,325);
		wendu.setScale(0.7);
		this.addChild(wendu);
		
		var cly_line1 = new cc.Sprite("#cly_line1.png");//水位线
		cly_line1.setPosition(-50,140);
		cly_line1.setScale(1.6,0.5);
		this.addChild(cly_line1);		
	},
	addqipao:function(pos,time){	//蒸馏烧瓶冒泡	
		var qipao = new cc.Sprite("#bubble1.png");
		qipao.setPosition(pos);
		this.addChild(qipao,2);
		var seq = cc.sequence(cc.spawn(cc.moveBy(time,cc.p(0,25)),cc.scaleTo(time,1.2)),cc.fadeOut(0.2),cc.callFunc(function(){
			qipao.removeFromParent(true);
		},this));
		qipao.runAction(seq);
	},
	addposX:function(){
		var posX = Math.random()*40;
		return (posX-70);		
	},
	genPoint:function(time){//接收器滴水
		var wp = new cc.Sprite("#wpoint.png");
		wp.setPosition(266, 70);
		wp.setScale(0.5);
		this.addChild(wp);
		var seq = cc.sequence(cc.spawn(cc.moveBy(time,cc.p(0,-8)),cc.scaleTo(time,0.6)),cc.moveBy(0.5,cc.p(0,-40)),cc.fadeOut(0.2),cc.callFunc(function(){
			wp.removeFromParent(true);
		},this));
		wp.runAction(seq);
	},
	addwater:function(){//锥形瓶水位
		water = new cc.Sprite("#cly_line1.png");
		water.setPosition(271, 0);
		water.setScale(0.3);
		this.addChild(water);
		var seq = cc.sequence(cc.spawn(cc.moveBy(20,cc.p(0,8)),cc.scaleTo(20,1.4,0.5)),cc.spawn(cc.moveBy(20,cc.p(0,8)),cc.scaleTo(20,1.3,0.6)));
		water.runAction(seq);
	},
	addwendu:function(){//温度
		var wendu = new cc.Sprite("#wd.png");
		wendu.setPosition(-50,240);
		wendu.setAnchorPoint(0.5,0);
		this.addChild(wendu);
		wendu.setScale(0.5,0.5);
		wendu.runAction(cc.scaleTo(5,0.5,30));
		
		
		var wendu1 = new cc.Sprite("#wd1.png");
		wendu1.setCascadeOpacityEnabled(true);
		wendu1.setPosition(30,350);
		this.addChild(wendu1);
		wendu1.setOpacity(0);
		var seq = cc.sequence(cc.delayTime(2),cc.fadeIn(0.5),cc.delayTime(5),cc.fadeOut(0.5));
		wendu1.runAction(seq);
		
		var wendu2 = new cc.Sprite("#wd.png");
		wendu2.setPosition(wendu1.width/2,0);
		wendu2.setAnchorPoint(0.5,0);
		wendu1.addChild(wendu2);
		wendu2.setScale(5,1);
		wendu2.runAction(cc.scaleTo(5,5,20));
	},
	addshuiwei:function(x,y,scalex){//冷凝管水位
		var shuiwei = new cc.Sprite("#arrow.png");
		shuiwei.setScale(scalex,0.05);
		shuiwei.setPosition(cc.p(x,y));
		shuiwei.setOpacity(100);
		this.addChild(shuiwei,5);
	},
	addarrow:function(pos,pos2){
		var arrow = new cc.Sprite("#arrow1.png");
		arrow.setPosition(pos);
		arrow.setScale(0.05);
		arrow.setRotation(-75);
		this.addChild(arrow);
		var label = new cc.LabelTTF("水","微软雅黑",25);
		label.setColor(cc.color(0, 0, 0));
		
		label.setPosition(pos2);
		this.addChild(label);
	},
	waterflow:function(){
		var frames=[];
		for(var i=1;i<5;i++){
			var str ="shuiliu/"+i+".png";
			var frame=cc.spriteFrameCache.getSpriteFrame(str);
			frames.push(frame);
		}
		var animation = new cc.Animation(frames,0.5);//负责动画序列
		var action = new cc.Animate(animation);//帧动画的动作创建
		return action;
	},

	
	stopAll:function(){
		this.pause();//暂停所有的调度过的选择器，动作和事件监听器。 这个方法被onExit方法在内部调用。 
	},
	callback:function(p){
		var action = gg.flow.flow.action;
		var func = cc.callFunc(this.actionDone, this);	
		func.retain();
		switch(p.getTag()){	
		case TAG_ZHENGLIU:
			break;

		case TAG_DOWN:
			this.addarrow(cc.p(130,120),cc.p(130,90));
			var water = new cc.Sprite("#cly_line1.png");
			water.setScale(0.3,0.5);
			water.setPosition(cc.p(147,155));
			this.addChild(water,5);
			var seq = cc.sequence(cc.spawn(cc.moveTo(1,cc.p(140,170)),cc.scaleTo(1,1.2,0.5)),cc.moveTo(2,cc.p(58,205)),
					cc.spawn(cc.moveTo(1,cc.p(47,215)),cc.scaleTo(1,0.6,0.5)),cc.spawn(cc.moveTo(0.3,cc.p(55,217)),cc.scaleTo(0.3,0.3,0.5)),
					cc.fadeOut(0.3),cc.callFunc(function(){
						//water.removeFromParent(true);
						this.flowNext();
					},this));
			water.runAction(seq);
			
			
			var delay = cc.delayTime(0.35);
			var seq1 = cc.sequence(delay,cc.callFunc(function(){
				this.addshuiwei(155,160,0.08);
			},this),delay,cc.callFunc(function(){
				this.addshuiwei(145,166,0.12);
			},this),delay,cc.callFunc(function(){
				this.addshuiwei(130,172,0.15);
			},this),delay,cc.callFunc(function(){
				this.addshuiwei(115,178,0.15);
			},this),delay,cc.callFunc(function(){
				this.addshuiwei(100,184,0.15);
			},this),delay,cc.callFunc(function(){
				this.addshuiwei(85,190,0.15);
			},this),delay,cc.callFunc(function(){
				this.addshuiwei(70,196,0.15);
			},this),delay,cc.callFunc(function(){
				this.addshuiwei(55,204,0.12);
			},this),delay,cc.callFunc(function(){
				this.addshuiwei(50,210,0.08);
			},this),delay,delay,cc.callFunc(function(){
				this.addarrow(cc.p(70,255),cc.p(75,285));
			},this));
			
			water.runAction(seq1);	
		break;
		}
	},

	actionDone:function(p){
		var action = gg.flow.flow.action;
		var func = cc.callFunc(this.actionDone, this);
		switch(p.getTag()){
		}
	},
	flowNext:function(){
		gg.flow.next();
	},

});