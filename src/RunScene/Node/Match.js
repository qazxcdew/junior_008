Match = cc.Node.extend({
	ctor:function(p){
		this._super();
		p.addChild(this, 10, TAG_MATCH_NODE);
		this.init();
	},
	init : function(){
		this.setCascadeOpacityEnabled(true);
		var match = new Button(this, 9, TAG_MATCH, "#lamp/match.png",this.callback);
		match.setRotation(60);
        var fire = new cc.Sprite("#lamp/fire.png");
        fire.setPosition(cc.p(-10,0));
        fire.setRotation(-70);
        match.addChild(fire);
	    hand.addrighthand(this,"#hand/hand_right",cc.p(30,-40)); 
        this.fire();
        
  
        
	},
	
	
	fireaction:function(){
		var frames=[];
		for (i=1;i<=3;i++){
			var str ="fire2/"+i+".png";
			var frame=cc.spriteFrameCache.getSpriteFrame(str);
			frames.push(frame);
		}
		
		var animation = new cc.Animation(frames,0.05);//负责动画序列
		var action = new cc.Animate(animation);//帧动画的动作创建
		return action;		
	},
	fire:function(){
		var lamp = this.getParent().getChildByTag(TAG_LAMPNODE_NODE).getChildByTag(TAG_LAMP_NODE);
		var fire2 = new cc.Sprite("#fire2/1.png");
		fire2.setAnchorPoint(0.5,0);
		fire2.setPosition(cc.p(0,23));
		fire2.setOpacity(0);
		lamp.addChild(fire2,11,TAG_SHOW);
		fire2.runAction(cc.repeatForever(this.fireaction()));
		var seq = cc.sequence(cc.delayTime(1),cc.fadeIn(0.5),cc.callFunc(function(){
			//this.flowNext();
			this.removeFromParent();
		},this));
		fire2.runAction(seq);
	},
	callback:function(p){
		var action = gg.flow.flow.action;
		var func = cc.callFunc(this.actionDone, this);	
		switch(p.getTag()){	
		}
	},
	flowNext:function(){
		gg.flow.next();
	},
});